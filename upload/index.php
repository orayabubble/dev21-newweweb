<?php
    include("lib/session.php");
    if ($_SESSION[$valSiteManage . "core_session_language"] == "") {
        $_SESSION[$valSiteManage . "core_session_language"] = "Thai";
        // $_SESSION[$valSiteManage . "core_session_language"] = "Eng";
    }

    include("lib/config.php");
    include("lib/connect.php");

    $_SESSION[$valSiteManage . "core_session_id"] = 0;
    $_SESSION[$valSiteManage . "core_session_name"] = "";
    $_SESSION[$valSiteManage . "core_session_level"] = "";
    $_SESSION[$valSiteManage . "core_session_language"] = "Thai";
    // $_SESSION[$valSiteManage . "core_session_language"] = "Eng";
    $_SESSION[$valSiteManage . "core_session_groupid"] = 0;
    $_SESSION[$valSiteManage . "core_session_permission"] = "";
    $_SESSION[$valSiteManage . "core_session_logout"] = "";

    include("core/incLang.php");
?>

<!DOCTYPE html>
<html lang="th">
<head>
    <title><?php echo $core_name_title; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="HandheldFriendly" content="true">
    <meta name="format-detection" content="telephone=no">

    <!-- META OPEN GRAPH (FB) -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta property="og:site_name" content="">
    <meta property="og:locale" content="">
    <meta property="og:locale:alternate" content="">

    <!-- TWITTER CARD -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:url" content="">
    <meta name="twitter:image:src" content="">

    <!-- ICONS -->
    <link rel="shortcut icon" href="assets/favicon/favicon.ico" type="image/x-icon"/>

    <link rel="stylesheet" type="text/css" href="assets/css/core.css">
    <link rel="stylesheet" type="text/css" href="assets/css/base.css">
    <link rel="stylesheet" type="text/css" href="assets/css/authen.css">
    <link rel="stylesheet" type="text/css" href="assets/css/theme-color.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font.css">
    <link rel="stylesheet" type="text/css" href="assets/css/feather.css">

    <script src="assets/js/jquery-2.1.4.min.js"></script>
    <script src="assets/js/jquery.easing-1.3.min.js"></script>
    <script src="assets/js/modernizr-3.6.0.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/validator.min.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="js/scriptCoreWeweb.js"></script>
</head>

<body>
    <div class="authentication" style="background: url('../upload/core/<?php echo $valPicBgSystem ?>') center;background-size: cover;">
        <div class="wrapper">
            <div class="inner">
                <div class="brand">
                    <div class="icon">
                        <img src="../upload/core/<?php echo $valPicSystem ?>" alt="">
                    </div>
                    <div class="name">
                        <div class="title"><?php echo $valNameSystem ?></div>
                        <div class="desc">Website Management<!-- <?php echo $valTitleSystem ?> --></div>
                    </div>
                </div>

                <form class="form-default" action="index.php" method="post" name="myFormLogin" id="myFormLogin">
                    <input id="inputUrl" name="inputUrl" type="hidden" value="<?php echo  $uID ?>">
                    <div class="form-group">
                        <label class="control-label">ชื่อผู้ใช้</label>
                        <input class="form-control" type="text" name="inputUser" id="inputUser" placeholder="">
                    </div>
                    <div class="form-group">
                        <label class="control-label">รหัสผ่าน</label>
                        <input class="form-control" type="password" name="inputPass" id="inputPass" placeholder="">
                    </div>
                    <div style="display: none;" class="alert-login" id="loadAlertLogin">
                        <span class="feather icon-alert-circle"></span>
                        <?php echo $langTxt["login:alert"] ?>
                    </div>
                    <div class="form-footer">
                        <input class="btn fluid btn-primary" name="input" type="submit" value="เข้าสู่ระบบ" />
                    </div>
                </form>

                <!-- <div class="copy">
                    Admin template by
                    <a href="https://www.wewebplus.com" class="link" target="_blank">Wewebplus Co.,Ltd.</a>
                </div> -->

                <div id="loadCheckComplete"></div>
            </div>
            
            <div style="display:none;" id="tallContent">
                <span>Please waiting..</span>
                <div style="height:10px;"></div>
                <img src="img/loader/login.gif" />
            </div>

            <?php wewebDisconnect($coreLanguageSQL); ?>
        </div>
    </div>

    <?php include('inc/loadscript.php'); ?>
    <script type="text/javascript">
        jQuery(function(){
            jQuery('form#myFormLogin').submit(function(){
                with (document.myFormLogin) {
                    if (inputUser.value == '') {
                        inputUser.focus();
                        return false;
                    }
                    if (inputPass.value == '') {
                        inputPass.focus();
                        return false;
                    }
                }
                checkLoginUser();
                return false;
            });
        });
    </script>
</body>
</html>