<?php
@include("../lib/session.php");
header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename="report_log_' . date('Y-m-d') . '.xls"'); #ชื่อไฟล์
include("../lib/config.php");
include("../lib/connect.php");
include("../lib/function.php");
include("../core/incLang.php");
include("incModLang.php");
include("config.php");

logs_access('3', 'Export');
?>
<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">


<HEAD>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <style type="text/css">
        <!--
        .bold {
            font-weight: bold;
        }
        -->
    </style>
</HEAD>

<BODY>
    <table border="1" cellspacing="1" cellpadding="2" align="left">
        <tbody>
            <tr>
                <td width="56" height="30" align="center" bgcolor="#eeeeee" class="bold" valign="middle"><?= $langMod["tit:no"] ?></td>
                <td width="175" align="center" bgcolor="#eeeeee" class="bold" valign="middle"><?= $langTxt["mg:subject"] ?></td>
                <td width="175" align="center" bgcolor="#eeeeee" class="bold" valign="middle">IP</td>
                <td width="175" align="center" bgcolor="#eeeeee" class="bold" valign="middle"><?= $langMod["tit:typeAccess"] ?></td>
                <td width="175" align="center" bgcolor="#eeeeee" class="bold" valign="middle"><?= $langTxt["home:date"] ?></td>
            </tr>

            <?
$sql = $_POST['sql_export'];
$query=wewebQueryDB($coreLanguageSQL,$sql) ;
$count_record=wewebNumRowsDB($coreLanguageSQL,$query);
$date_print=DateFormat(date("Y-m-d H:i:s"));
			if($count_record>=1){
			$index= 1;
			while($row=wewebFetchArrayDB($coreLanguageSQL,$query)) {
                $valID = $row['id'];
                $valName = rechangeQuot($row['filename']);
                $valDateCredate = dateFormatReal($row['credate']);
                $valTimeCredate = timeFormatReal($row['credate']);
                $valType = $row['type'];
                $valIp = $row['ip'];
                $valUrldownload = $row['urldownload'];
                        ?>

            <tr bgcolor="#ffffff">
                <td height="30" align="center" valign="middle"><?= $index ?></td>
                <td align="left" valign="middle"><strong><?= $valUrldownload ?></strong></td>
                <td align="center" valign="middle"><?= $valIp ?></td>
                <td align="center" valign="middle"><?= $valType ?></td>
                <td align="center" valign="middle"><?= $valDateCredate ?> <?= $valTimeCredate ?></td>
            </tr>

            <?
							$index++;
						} 
						$count_totalrecord= $count_record+1;
						}
				 ?>
        </tbody>
    </table>

    <table border="0" cellspacing="1" cellpadding="2" align="left">
        <tbody>
            <tr>
                <td width="175" align="right" valign="middle" class="bold">Print date : </td>
                <td width="175" align="left" valign="middle"><?= $date_print ?></td>
            </tr>
        </tbody>
    </table>
</BODY>

</HTML>