<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td class="divRightNavTb" align="left">
            <span class="fontContantTbNavPage">
                <?php echo $langTxt["pr:All"]?> 
                <b><?php echo number_format($count_totalrecord)?></b> 
                <?php echo $langTxt["pr:record"]?>
            </span>
        </td>
        <td class="divRightNavTb" align="right">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="right" style="padding-right:10px;">
                        <span class="fontContantTbNavPage"><?php echo $langTxt["pr:page"]?>
                            <?php if($numberofpage>1) { ?>
                            <select name="toolbarPageShow"  class="formSelectContantPage" onChange="document.myForm.module_pageshow.value=this.value; document.myForm.submit(); ">
                                <?php
                                    if($numberofpage<$module_default_maxpage) {
                                    	// Show page list #########################
                                    	for($i=1;$i<=$numberofpage;$i++) {
                                    		echo "<option value=\"$i\"";
                                    		if($i==$module_pageshow) { echo " selected"; }
                                    		echo ">$i</option>";
                                    	}

                                    }else {
                                    	// # If total page count greater than default max page  value then reduce page select size #########################
                                    	$starti = $module_pageshow-$module_default_reduce;
                                    	if($starti<1) { $starti=1; }
                                    	$endi = $module_pageshow+$module_default_reduce;
                                    	if($endi>$numberofpage) { $endi=$numberofpage; }
                                    	//#####################
                                    	for($i=$starti ;$i<=$endi;$i++) {
                                    		echo "<option value=\"$i\"";
                                    		if($i==$module_pageshow) { echo " selected"; }
                                    		echo ">$i</option>";
                                    	}
                                    }
                                ?>
                            </select>
                            <?php } else { ?>
                                <b><?php echo $module_pageshow?></b>
                            <?php }?>
                            <?php echo $langTxt["pr:of"]?> 
                            <b><?php echo $numberofpage?></b>
                        </span>
                    </td>
	                <?php if($module_pageshow>1) { ?>
                		<td width="21" align="center"> <img src="../img/controlpage/playset_start.gif" width="21" height="21"
                		  onmouseover="this.src='../img/controlpage/playset_start_active.gif'; this.style.cursor='hand';"
                		  onmouseout="this.src='../img/controlpage/playset_start.gif';"
                		  onclick="document.myForm.module_pageshow.value=1; document.myForm.submit();"  style="cursor:pointer;" />
                        </td>
            		<?php } else { ?>
                		<td width="21" align="center">
                            <img src="../img/controlpage/playset_start_disable.gif" width="21" height="21" />
                        </td>
	                <?php } ?>
	                <?php if($module_pageshow>1) {
	                    $valPrePage=$module_pageshow-1;
	                ?>
                		<td width="21" align="center"> <img src="../img/controlpage/playset_backward.gif" width="21" height="21"  style="cursor:pointer;"
                		    onmouseover="this.src='../img/controlpage/playset_backward_active.gif'; this.style.cursor='hand';"
                		    onmouseout="this.src='../img/controlpage/playset_backward.gif';"
                		    onclick="document.myForm.module_pageshow.value='<?php echo $valPrePage?>'; document.myForm.submit();" />
                        </td>
	                <?php } else { ?>
	                    <td width="21" align="center">
                            <img src="../img/controlpage/playset_backward_disable.gif" width="21" height="21" />
                        </td>
	                <?php } ?>
            		<td width="21" align="center"> <img src="../img/controlpage/playset_stop.gif" width="21" height="21" style="cursor:pointer;"
            		    onmouseover="this.src='../img/controlpage/playset_stop_active.gif'; this.style.cursor='hand';"
            		    onmouseout="this.src='../img/controlpage/playset_stop.gif';"
            		    onclick="
                		with(document.myForm) {
                		module_pageshow.value='';
                		module_pagesize.value='';
                		module_orderby.value='';
                        document.myForm.submit();
                		}
                		" />
                    </td>
            		<?php if($module_pageshow<$numberofpage) {
            		    $valNextPage=$module_pageshow+1;
            		?>
                		<td width="21" align="center"> <img src="../img/controlpage/playset_forward.gif" width="21" height="21" style="cursor:pointer;"
                		    onmouseover="this.src='../img/controlpage/playset_forward_active.gif'; this.style.cursor='hand';"
                		    onmouseout="this.src='../img/controlpage/playset_forward.gif';"
                		    onclick="document.myForm.module_pageshow.value='<?php echo $valNextPage?>'; document.myForm.submit();" />
                        </td>
	                <?php } else { ?>
	                    <td width="21" align="center">
                            <img src="../img/controlpage/playset_forward_disable.gif" width="21" height="21" />
                        </td>
	                <?php } ?>
	                <?php if($module_pageshow<$numberofpage) { ?>
                		<td width="21" align="center"><img src="../img/controlpage/playset_end.gif" width="21" height="21" style="cursor:pointer;"
                		    onmouseover="this.src='../img/controlpage/playset_end_active.gif'; this.style.cursor='hand';"
                		    onmouseout="this.src='../img/controlpage/playset_end.gif';"
                		    onclick="document.myForm.module_pageshow.value='<?php echo $numberofpage?>'; document.myForm.submit();" />
                        </td>
	                <?php } else { ?>
		                <td width="21" align="center">
                            <img src="../img/controlpage/playset_end_disable.gif" width="21" height="21" />
                        </td>
	                <?php } ?>
	            </tr>
	        </table>                        
        </td>
    </tr>
</table>