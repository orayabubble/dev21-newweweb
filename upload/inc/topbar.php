<!-- <?php include("../core/incLoderBox.php"); ?> -->
<div class="site-header">
    <div class="main">
        <div class="row row-0 align-items-center">
            <div class="col">
                <div class="brand">
                    <div class="row row-0 align-items-center">
                        <div class="col-auto">
                            <div class="icon">
                                <img src="<?php echo $core_pathname_crupload?>/<?php echo $valPicSystem?>" />
                            </div>
                        </div>
                        <div class="col">
                            <h2 class="title">
                                <?php echo $valNameSystem?>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="profile dropdown">
                    <a href="javascript:void(0);" class="link" data-toggle="dropdown">
                        <div class="row row-0 align-items-center">
                            <div class="col">
                                <h4 class="title">
                                    <?php echo  $_SESSION[$valSiteManage . "core_session_name"] ?>
                                </h4>
                            </div>
                            <div class="col-auto">
                                <div class="pic">
                                    <?php
                                        $valPicProfileTop = load_picmemberBack($_SESSION[$valSiteManage . "core_session_id"]);
                                        if (is_file($valPicProfileTop)) {
                                            $valPicProfileTop = $valPicProfileTop;
                                        } else {
                                            $valPicProfileTop = "../img/btn/nouser.jpg";
                                        }
                                        if ($_SESSION[$valSiteManage . "core_session_level"] == "SuperAdmin") {
                                            $valLinkViewUser = "#";
                                        } else {
                                            $valLinkViewUser = "../core/userView.php";
                                        }
                                    ?>
                                    <img src="<?php echo  $valPicProfileTop ?>" alt="">
                                </div>
                            </div>
                            <div class="col-auto">
                                <div class="icon">
                                    <span class="feather icon-chevron-down"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <ul class="nav-list fluid">
                            <?php if ($_SESSION[$valSiteManage . "core_session_groupid"] == "11" && $_SESSION[$valSiteManage . "core_session_typeusermini"] == 0) { ?>
                                <li>
                                    <a class="link" href="../core/userMiniManage.php" title="">
                                        <span class="feather icon-user"></span>
                                        <?php echo $langTxt["nav:userManage2"] ?>
                                    </a>
                                </li>
                            <?php }else{?>
                            <?php if ($_SESSION[$valSiteManage . "core_session_level"] == "SuperAdmin" || $_SESSION[$valSiteManage . "core_session_level"] == "admin") { ?>
                                <li>
                                    <a class="link" href="../core/userManage.php" title="">
                                        <span class="feather icon-user"></span>
                                        <?php echo $langTxt["nav:userManage2"] ?>
                                    </a>
                                </li>
                                <li>
                                    <a class="link" href="../core/permisManage.php">
                                        <span class="feather icon-settings"></span>
                                        <?php echo $langTxt["nav:perManage2"] ?>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if ($_SESSION[$valSiteManage . "core_session_level"] == "SuperAdmin") { ?>
                                <li>
                                    <a class="link" href="../core/menuManage.php">
                                        <span class="feather icon-list"></span>
                                        <?php echo $langTxt["nav:menuManage2"] ?>
                                    </a>
                                </li>
                                <li>
                                    <a class="link" href="../core/setting.php">
                                        <span class="feather icon-sidebar"></span>
                                        <?php echo $langTxt["nav:setting"] ?>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php } ?>
                            <li class="logout">
                                <a class="link" href="javascript:void(0)"onclick="checkLogoutUser();">
                                    <span class="feather icon-power"></span>
                                    <?php echo $langTxt["menu:logout"] ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input id="pathTojs" type="hidden" value="<?php echo $core_pathname_folderlocal;?>">