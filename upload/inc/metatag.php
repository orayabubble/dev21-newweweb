<title><?php echo $valNameSystem?> | Admin template by นั่งทำงาน</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> -->
<meta name="viewport" content="width=1366">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<meta name="HandheldFriendly" content="true">
<meta name="format-detection" content="telephone=no">

<!-- meta open graph (fb) -->
<meta property="og:type" content="website">
<meta property="og:url" content="">
<meta property="og:title" content="">
<meta property="og:description" content="">
<meta property="og:image" content="">
<meta property="og:site_name" content="">
<meta property="og:locale" content="">
<meta property="og:locale:alternate" content="">

<!-- twitter card -->
<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="">
<meta name="twitter:description" content="">
<meta name="twitter:url" content="">
<meta name="twitter:image:src" content="">

<!-- icons -->
<link rel="shortcut icon" href="../assets/favicon/favicon.ico" type="image/x-icon"/>