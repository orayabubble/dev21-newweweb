<div class="main">
    <div class="nav-group">
        <div class="nav-item <?php if ($menukeyid == '') { ?>divLeftMenuOver<?php } else { ?>divLeftMenu<?php } ?>" >
            <a href="../core/index.php" title="<?php echo  $langTxt["nav:home2"] ?>" class="<?php if ($menukeyid == '') { ?>fontContantB<?php } else { ?><?php } ?>">
                <span class="feather icon-home"></span>
                <?php echo  $langTxt["nav:home2"] ?>
            </a>
        </div>
    </div>

    <div class="nav-group">
        <h4 class="title-group">Modules</h4>
        <?php
        $sql_open = "SELECT  " . $core_tb_menu . "_parentid FROM " . $core_tb_menu . " WHERE " . $core_tb_menu . "_id='" . $menukeyid . "' AND " . $core_tb_menu . "_status='Enable'  AND ( " . $core_tb_menu . "_hidden!='Disable' or " . $core_tb_menu . "_hidden is null ) ORDER BY " . $core_tb_menu . "_order ASC ";
        $Query_open = wewebQueryDB($coreLanguageSQL, $sql_open);
        $Row_open = wewebFetchArrayDB($coreLanguageSQL, $Query_open);
        $status_open = $Row_open[0];
        
        $sql = "SELECT * FROM " . $core_tb_menu . " WHERE " . $core_tb_menu . "_parentid='0'  AND " . $core_tb_menu . "_status='Enable'  AND ( " . $core_tb_menu . "_hidden!='Disable' or " . $core_tb_menu . "_hidden is null )  ORDER BY " . $core_tb_menu . "_order ASC ";
        $Query = wewebQueryDB($coreLanguageSQL, $sql);
        $RecordCount = wewebNumRowsDB($coreLanguageSQL, $Query);

        if ($RecordCount >= 1) {

            while ($RowMenu = wewebFetchArrayDB($coreLanguageSQL, $Query)) {
                // print_pre($RowMenu);
                $masterkeyName = $RowMenu[$core_tb_menu . "_masterkey"];
                $myUserID = $_SESSION[$valSiteManage . "core_session_groupid"];
                $myMenuID = $RowMenu[$core_tb_menu . "_id"];
                $permissionID = getUserPermissionOnMenu($myUserID, $myMenuID);

                if ($RowMenu[$core_tb_menu . "_moduletype"] == "Module") {
                    $linkFileTo = $RowMenu[$core_tb_menu . "_linkpath"] . "?masterkey=" . $masterkeyName . "&amp;menukeyid=" . $myMenuID;
                    $linkTaget = "_self";
                } else if ($RowMenu[$core_tb_menu . "_moduletype"] == "Link") {
                    $linkFileTo = $RowMenu[$core_tb_menu . "_linkpath"];
                    $linkTaget = "_blank";
                }
                // print_pre($_SESSION[$valSiteManage . "core_session_level"]);
                if ($permissionID != "NA" || $_SESSION[$valSiteManage . "core_session_level"] == "SuperAdmin") {
                    if ($_SESSION[$valSiteManage . 'core_session_language'] == "Thai") {
                        $txt_menu_lan = $RowMenu[$core_tb_menu . "_namethai"];
                    } else if ($_SESSION[$valSiteManage . 'core_session_language'] == "Eng") {
                        $txt_menu_lan = $RowMenu[$core_tb_menu . "_nameeng"];
                    }

                    if ($RowMenu[$core_tb_menu . "_moduletype"] == "Group") {
                        $ParentID = $RowMenu[$core_tb_menu . "_id"];

                        $sql2 = "SELECT * FROM " . $core_tb_menu . " INNER JOIN " . $core_tb_permission . " ON " . $core_tb_menu . "." . $core_tb_menu . "_id =" . $core_tb_permission . "." . $core_tb_permission . "_menuid  WHERE " . $core_tb_menu . "_parentid='" . $ParentID . "'   AND " . $core_tb_menu . "_status='Enable'  AND ( " . $core_tb_menu . "_hidden!='Disable' or " . $core_tb_menu . "_hidden is null )  AND " . $core_tb_permission . "_perid='" . $_SESSION[$valSiteManage . 'core_session_groupid'] . "' AND " . $core_tb_permission . "_permission!='NA' ORDER BY " . $core_tb_menu . "_order ASC ";
                        $Query2 = wewebQueryDB($coreLanguageSQL, $sql2);
                        
                        $RecordCountSub = wewebNumRowsDB($coreLanguageSQL, $Query2);
                        $valHeightSub = $core_height_leftmenu * $RecordCountSub;
                        if ($status_open == $myMenuID) {
                            $valOpenNewMenu = "divLeftMenuOverNew";
                            $valNavImgNew = "../img/btn/navOpen.png";
                        } else {
                            $valOpenNewMenu = "divLeftMenu";
                            $valNavImgNew = "../img/btn/nav.png";
                        }
                        ?>
                        <div class="nav-item <?php echo $valOpenNewMenu ?>" id="boxMenuLeftShow<?php echo  $ParentID ?>" <?php if ($status_open == $ParentID) { ?>onclick="clickOutSubMenuLeft('boxMenuLeftShow<?php echo  $ParentID ?>', 'boxSubMenuLeftShow<?php echo  $ParentID ?>', '<?php echo  $valHeightSub ?>')"<?php } else { ?>onclick="clickInSubMenuLeft('boxMenuLeftShow<?php echo  $ParentID ?>', 'boxSubMenuLeftShow<?php echo  $ParentID ?>', '<?php echo  $valHeightSub ?>')"<?php } ?> style="cursor:pointer; width: 100%;">
                            <a href="javascript:void(0)" class="<?php if ($status_open == $myMenuID) { ?>fontContantB<?php } else { ?><?php } ?>">
                                <!-- <span class="fa">
                                    <?php if ($RowMenu[$core_tb_menu . "_icon"]) { ?><img src="<?php echo  $RowMenu[$core_tb_menu . "_icon"] ?>" border="0" align="absmiddle" /><?php
                                    } else {
                                        echo " - ";
                                    }
                                    ?>
                                </span> -->
                                <span class="feather icon-layers"></span>
                                <?php echo  $txt_menu_lan ?>
                                <span class="arrow"></span>
                            </a>
                        </div>
                        <?php
    					if ($_SESSION[$valSiteManage . "core_session_groupid"] == '11') { 
    						$valStyMiniUser='';
    					}else{
    						$valStyMiniUser='style="height:0px;display:none;"';
    					}
    					
    					?>
                        <div   <?php if ($status_open == $ParentID) { ?><?php } else { ?><?php echo $valStyMiniUser?><?php } ?>  id="boxSubMenuLeftShow<?php echo  $ParentID ?>" class="divmenu">
                            <?php
                            if ($RecordCountSub >= 1) {
                                ?>
                                <!--                    <div class="divLeftSubMenuEnd"></div>
                                -->                        <?php
                                while ($RowSub = wewebFetchArrayDB($coreLanguageSQL, $Query2)) {

                                    $masterkeyName = $RowSub[$core_tb_menu . "_masterkey"];
                                    $myUserID = $_SESSION[$valSiteManage . "core_session_groupid"];
                                    $myMenuID = $RowSub[$core_tb_menu . "_id"];
                                    $permissionID = getUserPermissionOnMenu($myUserID, $myMenuID);

                                    if ($RowSub[$core_tb_menu . "_moduletype"] == "Module") {
                                        $linkFileTo = $RowSub[$core_tb_menu . "_linkpath"] . "?masterkey=" . $masterkeyName . "&amp;menukeyid=" . $myMenuID;
                                        $linkTaget = "_self";
                                    } else if ($RowSub[$core_tb_menu . "_moduletype"] == "Link") {
                                        $linkFileTo = $RowSub[$core_tb_menu . "_linkpath"];
                                        $linkTaget = "_blank";
                                    }


                                    if ($_SESSION[$valSiteManage . 'core_session_language'] == "Thai") {
                                        $txt_menu_sublan = $RowSub[$core_tb_menu . "_namethai"];
                                    } else if ($_SESSION[$valSiteManage . 'core_session_language'] == "Eng") {
                                        $txt_menu_sublan = $RowSub[$core_tb_menu . "_nameeng"];
                                    }

                                    if ($permissionID != "NA" || $_SESSION[$valSiteManage . "core_session_level"] == "SuperAdmin") {
                                        ?>
                                        <div class="nav-item <?php if ($menukeyid == $myMenuID) { ?>divLeftSubMenuOver<?php } else { ?>divLeftSubMenu<?php } ?>"  >
                                            <a href="<?php echo  $linkFileTo ?>" target="<?php echo  $linkTaget ?>" class="<?php if ($menukeyid == $myMenuID) { ?>fontContantSubMenu<?php } else { ?><?php } ?>" >
                                                <?php echo  $txt_menu_sublan ?>
                                            </a>
                                        </div>
                                        <?php
                                    } // End if permission Sub Group
                                } //End  while  Sub Group
                            }  // End else Sub Group 
                            ?>
                        </div>

                    <?php } else { ?> 
                        <div class="nav-item <?php if ($menukeyid == $myMenuID) { ?>divLeftMenuOver<?php } else { ?>divLeftMenu<?php } ?>">
                            <a href="<?php echo  $linkFileTo ?>" target="<?php echo  $linkTaget ?>" class="<?php if ($menukeyid == $myMenuID) { ?>fontContantB<?php } else { ?><?php } ?>">
                                <!-- <span class="fa">
                                    <?php if ($RowMenu[$core_tb_menu . "_icon"]) { ?><img src="<?php echo  $RowMenu[$core_tb_menu . "_icon"] ?>" border="0" align="absmiddle"  /><?php
                                    } else {
                                        echo " - ";
                                    }
                                    ?>  
                                </span> -->
                                <span class="feather icon-layers"></span>
                                <?php echo  $txt_menu_lan ?>
                            </a>
                        </div>
                    <?php
                    } // End else Group
                } // End if permission
            } // End  while 
        } // End if >=1
        ?>
    </div>

    <!-- <div class="nav-group">
        <h4 class="title-group">Settings</h4>
    </div> -->
</div>