<div class="row row-0 align-items-center justify-content-center">
    <!-- <div class="col-auto">
        <div class="manage" title="<?php echo $langTxt["btn:top"] ?>" onclick="
            document.myFormHome.inputLt.value='Thai';
            document.myFormHome.valEditID.value=<?php echo $valID?>;
            editContactNew('topUpdateCategory.php');" >
            <div class="top">
                <span class="feather icon-arrow-up"></span>
            </div>
        </div>
    </div> -->
    <div class="col-auto">
        <div class="manage" title="<?php echo $langTxt["btn:edit"] ?>" onclick="
            document.myFormHome.inputLt.value = 'Thai';
            document.myFormHome.valEditID.value =<?php echo $valID ?>;
            editContactNew('editCategory.php');">
            <div class="edit editLang">
                <span class="feather icon-edit-1"></span>
                <span class="lang"><?php echo $langTxt["lg:thai"] ?></span>
            </div>
        </div>
    </div>
    <?php if ( $_SESSION[$valSiteManage . 'core_session_languageT'] == 2 || $_SESSION[$valSiteManage . 'core_session_languageT'] == 3 ) {?>
    <div class="col-auto">
        <div class="manage" title="<?php echo $langTxt["btn:edit"] ?>" onclick="
            document.myFormHome.inputLt.value = 'Eng';
            document.myFormHome.valEditID.value =<?php echo $valID ?>;
            editContactNew('editCategory.php');">
            <div class="edit editLang">
                <span class="feather icon-edit-1"></span>
                <span class="lang"><?php echo $langTxt["lg:eng"] ?></span>
            </div>
        </div>
    </div>
    <?php }?>
    <div class="col-auto">
        <div class="manage" title="<?php echo $langTxt["btn:del"] ?>"  onClick="
            if (confirm('<?php echo $langTxt["mg:delpermis"] ?>')) {
            Paging_CheckedThisItem(document.myForm.CheckBoxAll, <?php echo $index ?>, 'CheckBoxID', document.myForm.TotalCheckBoxID.value);
            delContactNew('deleteCategory.php');}">
            <div class="remove">
                <span class="feather icon-trash-2"></span>
            </div>
        </div>
    </div>
</div>